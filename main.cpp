#include <iostream>
#include <thread>
#include <chrono>
#include "Game.h"
#include <mutex>
#include <csignal>
#include <termios.h>

int main(int /* argc */, char **argv) {
    auto inputThread = [](Game &game, bool &isComputeThreadReady, bool &isOutputThreadReady) {
        std::cout << "\rStart input thread\n";
        while (!game.getIsTerminated()) {
                char input;
                read(0, &input, 1);
                if (game.getIsGameOver() && (input == 'r' || input == 'R')) {
                    game.restartGame();
                    isComputeThreadReady = false;
                    isOutputThreadReady = true;
                } else if (input == 'q' || input == 'Q') {
                    game.setIsTerminated(true);
                } else {
                    game.controlSnake(input);
                }

        }
        std::cout << "\rExit input thread\n";
    };
    auto computeThread = [](Game &game, bool &isComputeThreadReady, bool &isOutputThreadReady) {
        std::cout << "\rStart compute thread\n";

        while (!game.getIsTerminated()) {
            if (!isOutputThreadReady) {
                continue;
            }
            isOutputThreadReady = false;
            game.update();
            isComputeThreadReady = true;
            std::this_thread::sleep_for(std::chrono::milliseconds(game.getDelay()));
        }

        std::cout << "\rExit compute thread\n";
    };
    auto outputThread = [](Game &game, bool &isComputeThreadReady, bool &isOutputThreadReady) {
        std::cout << "\rStart output thread\n";
//        std::unique_lock<std::mutex> ul(mutex);

        while (!game.getIsTerminated()) {
            if (!isComputeThreadReady) {
                continue;
            }
            isComputeThreadReady = false;
            system("clear");
            if (game.getIsGameOver()) {
                std::cout << "Game is over. Your score: " + std::to_string(game.getPoints())
                          << ". Press R to restart game." << '\n';
            } else {
                std::cout << "Score: " + std::to_string(game.getPoints()) << '\n';
                std::cout << game.getOutputText();
                isOutputThreadReady = true;
            }
        }

        std::cout << "\rExit output thread\n";
    };

    auto rowCount = strtol(argv[1], nullptr, 0);
    auto colCount = strtol(argv[2], nullptr, 0);
    auto delay = strtol(argv[3], nullptr, 0);

    auto isComputeThreadReady = false;
    auto isOutputThreadReady = true;

    Game game = Game(rowCount, colCount, delay);

    std::thread t1(inputThread, std::ref(game), std::ref(isComputeThreadReady), std::ref(isOutputThreadReady));
    std::thread t2(computeThread, std::ref(game), std::ref(isComputeThreadReady), std::ref(isOutputThreadReady));
    std::thread t3(outputThread, std::ref(game), std::ref(isComputeThreadReady), std::ref(isOutputThreadReady));

    static struct termios oldt, newt;

    // Setting the terminal so that it accepts input without pressing the enter key
    tcgetattr( STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON);
    tcsetattr( STDIN_FILENO, TCSANOW, &newt);

    t1.join();
    t2.join();
    t3.join();

    std::cout << "\rExit main thread\n";

    // Restore the old settings
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt);

    return 0;
}






