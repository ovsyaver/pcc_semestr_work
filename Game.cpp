//
// Created by Veronika on 10-Jan-23.
//

#include <random>
#include "Game.h"
#include "Board.h"

#define COLOR_RESET "\x1b[0m"
#define COLOR_GREEN "\x1b[32m"
#define COLOR_MAGENTA "\x1b[95m"
#define COLOR_BLUE "\x1b[94m"

// Set up the game with custom parameters.
// @param _rowCount, _colCount - size of the game screen
// @param _delay - delay before snake will move, in milliseconds
Game::Game(size_t _rowCount, size_t _colCount, size_t _delay) : rowCount(_rowCount),

                                                             colCount(_colCount), delay(_delay) {
    isGameOver = false;
    shouldGrow = false;
    isTerminated = false;
    initGame();
}

void Game::initGame() {
    score = 0;
    board = std::make_unique<Board>(rowCount, colCount);
    auto cellForSnake = board->getCell(rowCount / 2, colCount / 2);
    snake = std::make_unique<Snake>(cellForSnake);
    generateFood();
}

void Game::update() {
    if (!isGameOver) {
        auto nextCell = findNextCell();
        auto nextCellType = nextCell->getCellType();

        if (nextCellType == Cell::CellType::SNAKE || nextCellType == Cell::CellType::WALL) {
            isGameOver = true;
        } else {
            snake->move(nextCell, shouldGrow);
            shouldGrow = false;

            if (nextCellType == Cell::CellType::FOOD) {
                ++score;
                shouldGrow = true;
                generateFood();
            }
        }
    }
}

// Find next cell in the direction of the snake
Cell *Game::findNextCell() {
    auto curRow = snake->getHead()->getRow();
    auto curCol = snake->getHead()->getCol();
    auto dir = snake->getDirection();

    if (dir == Snake::Direction::RIGHT) {
        curCol++;
    } else if (dir == Snake::Direction::LEFT) {
        curCol--;
    } else if (dir == Snake::Direction::UP) {
        curRow--;
    } else if (dir == Snake::Direction::DOWN) {
        curRow++;
    }

    auto nextCell = board->getCell(curRow, curCol);

    return nextCell;
}

// Find the free cell that is not the snake and is not the wall
void Game::generateFood() {
    while (true) {
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_int_distribution<std::mt19937::result_type> randomRowNumber(0, rowCount - 1);
        std::uniform_int_distribution<std::mt19937::result_type> randomColNumber(0, colCount - 1);
        size_t row = randomRowNumber(rng);
        size_t column = randomColNumber(rng);
        auto cell = board->getCell(row, column);

        if (cell->getCellType() != Cell::CellType::SNAKE && cell->getCellType() != Cell::CellType::WALL) {
            cell->setCellType(Cell::CellType::FOOD);
            break;
        }
    }
}

size_t Game::getDelay() const {
    return delay;
}

size_t Game::getPoints() const {
    return score;
}

void Game::restartGame() {
    isGameOver = false;
    isTerminated = false;
    shouldGrow = false;
    initGame();
}

// Get cell symbol to render game screen
char Game::getSymbolByCellType(Cell *cell) const {
    switch (cell->getCellType()) {
        case Cell::CellType::SNAKE:
            return  snakeSymbol;
        case Cell::CellType::FOOD:
            return foodSymbol;
        case Cell::CellType::WALL:
            return wallSymbol;
        default:
            return ' ';
    }
}

// Get text to render game screen
std::string Game::getOutputText() {
    auto cells = board->getCells();
    std::string outputText = std::string();
    for (const auto &row: cells) {
        for (auto col: row) {
            outputText += getColorByCellTYpe(col);
            outputText += getSymbolByCellType(col);
            outputText += COLOR_RESET;
        }
        outputText += '\n';
    }
    return outputText;
}

void Game::controlSnake(int key) {
    switch (key) {
        case 'W':
        case 'w': {
            snake->setDirection(Snake::Direction::UP);
            break;
        }
        case 'S':
        case 's': {
            snake->setDirection(Snake::Direction::DOWN);
            break;
        }
        case 'A':
        case 'a': {
            snake->setDirection(Snake::Direction::LEFT);
            break;
        }
        case 'D':
        case 'd': {
            snake->setDirection(Snake::Direction::RIGHT);
            break;
        }
        default: {
            break;
        }
    }
}

bool Game::getIsGameOver() const {
    return isGameOver;
}

void Game::setIsTerminated(bool _isTerminated) {
    isTerminated = _isTerminated;
}

bool Game::getIsTerminated() const {
    return isTerminated;
}

std::string Game::getColorByCellTYpe(Cell *cell) {
    auto cellType = cell->getCellType();
    switch (cellType) {
        case Cell::CellType::SNAKE:
            return COLOR_BLUE;
        case Cell::CellType::FOOD:
            return COLOR_GREEN;
        case Cell::CellType::WALL:
            return COLOR_MAGENTA;
        default:
            return "";
    }
}


