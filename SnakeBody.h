//
// Created by Veronika on 11-Jan-23.
//

#ifndef snakeGame_SNAKEBODY_H
#define snakeGame_SNAKEBODY_H


#include <memory>
#include "Cell.h"

// Linked list for snake body
class SnakeBody {
public:
    explicit SnakeBody(Cell* head);
    ~SnakeBody();

    struct Node
    {
        Cell* data;
        struct Node *next;
    };
    void addHead (Cell* nodeData);
    void removeTail();
    Cell* getHead();
private:
    struct Node* head;
};

#endif //snakeGame_SNAKEBODY_H
