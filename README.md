# Snake game

## Zadání
Hra pro LINUX terminal (udělaná na UBUNTU 20.4).

Implementujte hru klasickou hru Housenka.
Housenka pohybuje se po ploše a sbírá zelí.
Když to zelí sobere, vyroste ji odsas.
Pokud se srazí s částí svého těla nebo okrajem herní obrazovky hra končí.


## Kompilace programu
Program lze zkompilovat následujícím příkazem:
```bash
cmake -Bcmake-build-debug -H. -DCMAKE_BUILD_TYPE=Debug
cmake --build cmake-build-debug
```

## Spuštění a přepínače
Hra se spouští z příkazové řádky.

Parametry nastavení programu:
- první parametr - počet řádků hracího pole,
- druhy parametr - počet sloupců hracího pole,
- třetí parametr - zpoždění, než se housenka znovu pohne (milliseconds).

Příklady spuštění programu: 
```bash
./snakeGame 20 30 500
```
Tento příkaz spoustí hru s hracím polem na 20 řadků a 30 sloupců a se zpožděním 500 milliseconds.

## Ovládání programu
Hra se ovládá klasické.
- `A` - housenka se posune doleva
- `W` - housenka se posune nahoru
- `S` - housenka se posune dolu
- `D` - housenka se posune doprava

### Restartování hry
Když hra ukončí je možné zmačknut klávisu `R` a hra se spoustí znovu.

### Ukončení programu
Program je možné kdykoliv ukončit klávesou `Q`.

## Testování programu

### Příklad testů pro hru Snake game:
- Když housenka se srazí s částí svého těla nebo okrajem herní obrazovky (symbol `#`) - hra ukončí, bude vypsán počet scorů, které hráč ziskal a prográm nabídně restartování hry.
- Když housenka se srazí s jídlem (symbol `$`), jí se přidá nová část těla a hráč dostane + 1 score.