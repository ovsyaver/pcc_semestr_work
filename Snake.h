//
// Created by Veronika on 10-Jan-23.
//

#ifndef snakeGame_SNAKE_H
#define snakeGame_SNAKE_H

#include <memory>
#include "Cell.h"
#include "SnakeBody.h"

class Snake {

public:
    explicit Snake(Cell* head);

    enum class Direction {
        UP, DOWN, LEFT, RIGHT
    };

    Cell* getHead();
    void setDirection(Direction _direction);
    Direction getDirection() const;
    void move(Cell* nextCell, bool isGrowing);

private:
    Direction direction;
    SnakeBody snakeBody;
};




#endif //snakeGame_SNAKE_H
