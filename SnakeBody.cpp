//
// Created by Veronika on 11-Jan-23.
//
#include "SnakeBody.h"

SnakeBody::SnakeBody(Cell *head) {
    auto *headNode = new Node;
    headNode->data = head;
    headNode->next = nullptr;
    headNode->data->setCellType(Cell::CellType::SNAKE);

    this->head = headNode;
}

SnakeBody::~SnakeBody() {

    struct Node* tmp;

    while (head != nullptr)
    {
        tmp = head;
        head = head->next;
        delete tmp;
    }
}

void SnakeBody::addHead(Cell *nodeData) {
    auto *newHead = new Node;

    newHead->data = nodeData;
    newHead->next = head;
    newHead->data->setCellType(Cell::CellType::SNAKE);

    head = newHead;
}

void SnakeBody::removeTail() {
    struct Node *last = head;

    while (last->next->next != nullptr)
        last = last->next;

    last->next->data->setCellType(Cell::CellType::EMPTY);
    last->next = nullptr;
}

Cell *SnakeBody::getHead() {
    return head->data;
}


