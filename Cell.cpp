//
// Created by Veronika on 11-Jan-23.
//

#include "Cell.h"

Cell::Cell(size_t _row, size_t _col) : row(_row), col(_col) {
    cellType = CellType::EMPTY;
}

void Cell::setCellType(Cell::CellType _cellType) {
    Cell::cellType = _cellType;
}

size_t Cell::getRow() const {
    return row;
}

size_t Cell::getCol() const {
    return col;
}

Cell::CellType Cell::getCellType() const {
    return cellType;
}
