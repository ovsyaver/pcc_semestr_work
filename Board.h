//
// Created by Veronika on 10-Jan-23.
//

#ifndef snakeGame_BOARD_H
#define snakeGame_BOARD_H


#include <vector>
#include <memory>
#include "Cell.h"

class Board {
public:
    Board(size_t rowCount, size_t colCount);
    ~Board();
    void initBoard();

    Cell* getCell(size_t row, size_t col);
    std::vector<std::vector<Cell*>> getCells();

private:
    std::vector<std::vector<Cell*>> cells;
    const size_t rowCount;
    const size_t colCount;
};


#endif //snakeGame_BOARD_H
