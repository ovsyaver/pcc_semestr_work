//
// Created by Veronika on 11-Jan-23.
//

#ifndef snakeGame_CELL_H
#define snakeGame_CELL_H


#include <cstddef>

class Cell {
public:
Cell(size_t _row, size_t _col);

enum class CellType {
    EMPTY, FOOD, SNAKE, WALL
};

void setCellType(CellType _cellType);

    size_t getRow() const;

    size_t getCol() const;

    CellType getCellType() const;

private:
    const size_t row, col;
    CellType cellType;
};


#endif //snakeGame_CELL_H
