//
// Created by Veronika on 10-Jan-23.
//

#include "Snake.h"
#include "Cell.h"

Snake::Snake(Cell* head) : snakeBody(head){
    direction = Direction::UP;
}
// Snake is moving in the way that it's cell for tail is become an empty cell
// and cell in front of it's head become a new part of snake body
void Snake::move(Cell* nextCell, bool isGrowing) {
    snakeBody.addHead(nextCell);
    if (isGrowing) {
        return;
    }
    snakeBody.removeTail();
}

void Snake::setDirection(Direction _direction) {
    this->direction = _direction;
}

Snake::Direction Snake::getDirection() const {
    return direction;
}

Cell *Snake::getHead() {
    return snakeBody.getHead();
}




