//
// Created by Veronika on 10-Jan-23.
//
#ifndef snakeGame_GAME_H
#define snakeGame_GAME_H

#include <memory>

#include "Cell.h"
#include "Board.h"
#include "Snake.h"


class Game {

public:
    Game(size_t row, size_t col, size_t _delay);

    void update();
    void controlSnake(int key);
    void restartGame();

    std::string getOutputText();

    Cell* findNextCell();

    void generateFood();

    size_t getDelay() const;

    size_t getPoints() const;

    bool getIsGameOver() const;

private:
    void initGame();
    char getSymbolByCellType(Cell* cell) const;
    static std::string getColorByCellTYpe(Cell* cell) ;
public:
    bool getIsTerminated() const;

    void setIsTerminated(bool isTerminated);

private:
    const size_t rowCount, colCount, delay;
    bool isGameOver;
    bool shouldGrow;
    bool isTerminated;
    size_t score = 0;

    const char foodSymbol = '$';
    const char snakeSymbol = '@';
    const char wallSymbol = '#';

    std::unique_ptr<Snake> snake;
    std::unique_ptr<Board> board;
};

#endif
