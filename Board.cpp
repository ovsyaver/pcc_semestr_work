//
// Created by Veronika on 10-Jan-23.
//

#include "Board.h"
Board::Board(size_t _rowCount, size_t _colCount) : rowCount(_rowCount), colCount(_colCount) {
    cells = std::vector<std::vector<Cell*>>(rowCount);
    initBoard();
}

// Create cells for the game board
void Board::initBoard() {
    for (int row = 0; row < rowCount; row++) {
        cells.at(row) = std::vector<Cell*>(colCount);
        for (int column = 0; column < colCount; column++) {
            cells.at(row).at(column) = new Cell(row, column);
        }
    }

    // Mark the boundaries
    for (const auto& cell : cells.at(0))  {
        cell->setCellType(Cell::CellType::WALL);
    }

    for (const auto& cell : cells.back()) {
        cell->setCellType(Cell::CellType::WALL);
    }

    for (size_t i = 0; i < rowCount; ++i) {
        auto cell = cells.at(i).at(0);
        cell->setCellType(Cell::CellType::WALL);

        cell = cells.at(i).back();
        cell->setCellType(Cell::CellType::WALL);
    }
}

Cell* Board::getCell(size_t row, size_t col) {
    return cells.at(row).at(col);
}

std::vector<std::vector<Cell *>> Board::getCells() {
    return cells;
}

Board::~Board() {
    for (const auto& row : cells) {
        for (auto col : row) {
            delete col;
        }
    }
}
